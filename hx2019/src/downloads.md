# downloads

It will take lengthy time to download and install below softwares, especially over slow network.

Please download and install them in advance.

## mac
- [iTerm2](https://iterm2.com)
- [Brew](https://brew.sh)
```
brew install git tig tree vim tmux
```
- [Xcode](https://developer.apple.com/xcode/) (via Mac App Store)
- VM (optional, see below in windows)

## windows
- VM
	- [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
	- [PureOS ISO](https://www.pureos.net/download)
	- install Rust and IDE (Android Studio) in VM (see below)
- [Git SCM](https://git-scm.com/downloads)
- [Cygwin](https://www.cygwin.com)
	- [apt-cyg](https://github.com/transcode-open/apt-cyg)
```
apt-cyg install git tig tree vim tmux
```
- [ConEmu](https://github.com/Maximus5/ConEmu)

## gnu/linux (or PureOS inside VM)
- [Terminator](https://gnometerminator.blogspot.com/p/introduction.html)
```
sudo apt install git tig tree vim tmux
```

## common softwares across all systems

For mac, download and install below in native system. (optional in PureOS VM guest to try out gnu/linux)

For windows, install below in PureOS MV guest, also in native windows if disk space allowed.



- Rust
	- [rustup](https://rustup.rs)
	- [mdbook](https://github.com/rust-lang-nursery/mdBook)
```
cargo install mdbook
```

- IDE
	- [Toolbox App](https://www.jetbrains.com/toolbox/app/)
	- [IntelliJ IDEA](https://www.jetbrains.com/idea/)
		- [IntelliJ Rust](https://intellij-rust.github.io)
	- [Android Studio](https://developer.android.com/studio)
	- [Atom](https://atom.io)

- Browsers:
	- [Firefox](https://www.mozilla.org/)
	- [Chromiumn](https://www.chromium.org/getting-involved/download-chromium)
	- [Brave](https://brave.com)

## operating system difference
We will mainly focus on [POSIX](https://en.wikipedia.org/wiki/POSIX) environment, mac and PureOS (in VM) share lots of similarity in this regard for shell commands.

While mac has Xcode to build Local Native iOS app, windows has to deal with Registry Editor to make Local Native web extension work.
